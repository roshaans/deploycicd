const express = require("express")

const app = express()

const port = 8000

app.get("/",(req,res)=>{
    res.send("This is sample CICD pipeline")
})

app.listen(port,()=>{
    console.log("Server running at port 8000")
})